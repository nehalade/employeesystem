package com.employee.EmployeSystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author neha.valmik lade
 *
 */
@SpringBootApplication
public class EmployeSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmployeSystemApplication.class, args);
	}

}
