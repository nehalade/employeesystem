package com.employee.EmployeSystem.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.employee.EmployeSystem.entity.Employee;

/**
 * 
 * @author neha.valmik lade
 *
 */
@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer>{

}
