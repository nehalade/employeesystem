package com.employee.EmployeSystem.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.hibernate.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.employee.EmployeSystem.entity.Employee;
import com.employee.EmployeSystem.service.EmployeeService;
/**
 * 
 * @author neha.valmik lade
 *
 */

@ Controller
public class EmpConroller {
	
	@Autowired
	private EmployeeService service;
	
	@GetMapping("/")
	public String home(Model m) {
		
		List<Employee> emp=service.getAllEmp();
		m.addAttribute("emp",emp);
		
		return "index";
	}
	
		@GetMapping("/add_course")
		public String addEmpForm() {
			return "add_course";
		}
		
		

		@GetMapping("/contactus")
		public String contactEmpForm() {
			return "contactus";
		}
		
		
			@PostMapping("/register")
			public String empRegister(@ModelAttribute Employee e,HttpSession session) {
				
				System.out.println(e);
				
				service.addEmp(e);
				
				session.setAttribute("msg","course Added successfully!...");
				
				return "redirect:/";
		
			}
			
			@GetMapping("/edit/{id}")
			public String edit(@PathVariable int id,Model m)
			{
				Employee e=service.getEMpById(id);
				
				m.addAttribute("emp",e);
				
				return "edit";
			}
			
			@PostMapping("/update")
			public String updateEmp(@ModelAttribute Employee e,HttpSession session)
			{
				service.addEmp(e);
				session.setAttribute("msg","course Data Update Successfully..");
				return "redirect:/";
			}
			
			@GetMapping("/delete/{id}")
			public String deleteEMp(@PathVariable int id,HttpSession session)
			{
				service.deleteEMp(id);
				session.setAttribute("msg","course Data Delete Successfully..");
				return "redirect:/";
			}
			}
	


